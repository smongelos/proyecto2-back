package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.OrdenesCompraDetallesDao;
import com.bellaUtopia.entidad.OrdenesCompraDetalles;

public class OrdenesCompraDetallesService extends ServiceBase<OrdenesCompraDetalles, DaoBase<OrdenesCompraDetalles>> {

	@Inject
	private OrdenesCompraDetallesDao dao;

	@Override
	public DaoBase<OrdenesCompraDetalles> getDao() {
		return dao;
	}
}