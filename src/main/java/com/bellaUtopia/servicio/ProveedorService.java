package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.ProveedorDao;
import com.bellaUtopia.entidad.Proveedor;

public class ProveedorService extends ServiceBase<Proveedor, DaoBase<Proveedor>> {

	@Inject
	private ProveedorDao dao;

	@Override
	public DaoBase<Proveedor> getDao() {
		return dao;
	}
}