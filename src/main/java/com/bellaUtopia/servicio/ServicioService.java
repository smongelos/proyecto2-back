package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.ServicioDao;
import com.bellaUtopia.entidad.Servicio;

public class ServicioService extends ServiceBase<Servicio, DaoBase<Servicio>> {

	@Inject
	private ServicioDao dao;

	@Override
	public DaoBase<Servicio> getDao() {
		return dao;
	}
}