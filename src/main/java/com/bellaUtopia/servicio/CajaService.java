package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.CajaDao;
import com.bellaUtopia.entidad.Caja;

public class CajaService extends ServiceBase<Caja, DaoBase<Caja>> {

	@Inject
	private CajaDao dao;

	@Override
	public DaoBase<Caja> getDao() {
		return dao;
	}
}