package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.EmpleadoDetallesDao;
import com.bellaUtopia.entidad.EmpleadoDetalles;

public class EmpleadoDetallesService extends ServiceBase<EmpleadoDetalles, DaoBase<EmpleadoDetalles>> {

	@Inject
	private EmpleadoDetallesDao dao;

	@Override
	public DaoBase<EmpleadoDetalles> getDao() {
		return dao;
	}
}