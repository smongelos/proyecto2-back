package com.bellaUtopia.servicio;

import javax.inject.Inject;

import com.bellaUtopia.dao.EmpleadoDao;
import com.bellaUtopia.entidad.Empleado;
import com.bellaUtopia.entidad.EmpleadoDetalles;
import com.bellaUtopia.param.EmpleadoDetallesParam;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

public class EmpleadoService extends ServiceBase<Empleado, DaoBase<Empleado>> {

	@Inject
	private EmpleadoDao dao;
	
	@Inject
	private EmpleadoDetallesService service;

	@Override
	public DaoBase<Empleado> getDao() {
		return dao;
	}

	public EmpleadoDetallesParam insertarEmpleadoDetalles(EmpleadoDetallesParam empleadoDetallesParam) {
		
		/**
		 * INSERTAR EMPLEADO
		 */
		
		Empleado empleado = new Empleado();
		
		empleado.setIdPersona(empleadoDetallesParam.getIdPersona());
		empleado.setFechaIngreso(empleadoDetallesParam.getFechaIngreso());
		empleado.setFechaSalida(empleadoDetallesParam.getFechaSalida());
		empleado.setUsuario(empleadoDetallesParam.getUsuario());
		dao.insert(empleado);
		empleadoDetallesParam.setIdEmpleado(empleado.getIdEmpleado());
		

		/**
		 * INSERTAR EMPLEADO DETALLES
		 */
		
		EmpleadoDetalles empleadoDetalles = new EmpleadoDetalles();
		empleadoDetalles.setIdEmpleado(empleado.getIdEmpleado());
		empleadoDetalles.setSalario(empleadoDetallesParam.getSalario());
		empleadoDetalles.setIdRubro(empleadoDetallesParam.getIdRubro());
		empleadoDetalles.setFechaAsignacion(empleadoDetallesParam.getFechaAsignacion());
		service.insertar(empleadoDetalles);
		empleadoDetallesParam.setIdEmpleadoDetalles(empleadoDetalles.getIdEmpleadoDetalles());
		
		return empleadoDetallesParam;
	}
	
	public EmpleadoDetallesParam modificarEmpleadoDetalles(EmpleadoDetallesParam empleadoDetallesParam) {
		
		/**
		 * MODIFICAR EMPLEADO
		 */
		
		Empleado empleado = new Empleado();
		
		empleado.setIdPersona(empleadoDetallesParam.getIdPersona());
		empleado.setFechaIngreso(empleadoDetallesParam.getFechaIngreso());
		empleado.setFechaSalida(empleadoDetallesParam.getFechaSalida());
		empleado.setUsuario(empleadoDetallesParam.getUsuario());
		empleado.setIdEmpleado(empleadoDetallesParam.getIdEmpleado());
		dao.modificar(empleado);		

		/**
		 * MODIFICAR EMPLEADO DETALLES
		 */
		
		EmpleadoDetalles empleadoDetalles = new EmpleadoDetalles();
		empleadoDetalles.setIdEmpleado(empleadoDetallesParam.getIdEmpleado());
		empleadoDetalles.setSalario(empleadoDetallesParam.getSalario());
		empleadoDetalles.setIdRubro(empleadoDetallesParam.getIdRubro());
		empleadoDetalles.setFechaAsignacion(empleadoDetallesParam.getFechaAsignacion());
		empleadoDetalles.setIdEmpleadoDetalles(empleadoDetallesParam.getIdEmpleadoDetalles());
		
		service.modificar(empleadoDetalles);
		
		return empleadoDetallesParam;
	}
}