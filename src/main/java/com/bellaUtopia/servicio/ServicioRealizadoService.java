package com.bellaUtopia.servicio;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.bellaUtopia.dao.CajaDao;
import com.bellaUtopia.dao.FacturaDao;
import com.bellaUtopia.dao.OperacionesDao;
import com.bellaUtopia.dao.ServicioRealizadoDao;
import com.bellaUtopia.entidad.Caja;
import com.bellaUtopia.entidad.Factura;
import com.bellaUtopia.entidad.Operaciones;
import com.bellaUtopia.entidad.ServicioRealizado;
import com.bellaUtopia.excepciones.ApplicationException;
import com.bellaUtopia.param.CobranzaParam;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

@Stateless
public class ServicioRealizadoService extends ServiceBase<ServicioRealizado, DaoBase<ServicioRealizado>> {

	@Inject
	private ServicioRealizadoDao dao;

	@Inject
	private FacturaDao facturaDao;

	@Inject
	private OperacionesDao operacionesDao;

	@Inject
	private CajaDao cajaDao;

	@Override
	public DaoBase<ServicioRealizado> getDao() {
		return dao;
	}

	public CobranzaParam realizarPago(CobranzaParam cobranzaParam) {

		try {

			ServicioRealizado servicioRealizado =  new ServicioRealizado();
			servicioRealizado = dao.obtenerEntidad(cobranzaParam.getServicioRealizado().getIdServicioRealizado(), ServicioRealizado.class);
			
			//Caja caja = cobranzaParam.getCaja();

			Caja caja = cajaDao.obtenerEntidad(cobranzaParam.getCaja().getIdCaja(), Caja.class );

			Factura factura = new Factura();
			factura.setIdCaja(caja.getIdCaja());
			factura.setIdServicioRealizado(servicioRealizado.getIdServicioRealizado());
			factura.setMontoTotal(servicioRealizado.getMontoTotal());
			factura.setIva10(servicioRealizado.getMontoTotal() / 11);
			factura.setExenta(0);
			factura.setFecha(new Date());
			factura.setIva5(0);

			factura = facturaDao.insert(factura);

			Operaciones operaciones = new Operaciones();
			operaciones.setIdCaja(caja.getIdCaja());
			operaciones.setIdServicioRealizado(servicioRealizado.getIdServicioRealizado());
			operaciones.setMonto(servicioRealizado.getMontoTotal());
			operaciones.setTipoOperacion("E");

			operacionesDao.insert(operaciones);

			// ACTUALIZAR EL MONTO TOTAL DE LA CAJA
			caja.setMontoTotalIngreso(caja.getMontoTotalIngreso() + servicioRealizado.getMontoTotal());

			cajaDao.modificar(caja);

			// MARCAR SERVICIO REALIZADO COMO PAGADO

			servicioRealizado.setPagado(true);
			dao.modificar(servicioRealizado);
			cobranzaParam.setFactura(factura);

		} catch (Exception e) {
			throw new ApplicationException("No se pudo realizar el cobro del servicio realizado", e.getMessage());
		}

		return cobranzaParam;
	}
}