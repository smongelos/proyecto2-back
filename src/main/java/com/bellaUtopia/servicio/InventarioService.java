package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.InventarioDao;
import com.bellaUtopia.entidad.Inventario;

public class InventarioService extends ServiceBase<Inventario, DaoBase<Inventario>> {

	@Inject
	private InventarioDao dao;

	@Override
	public DaoBase<Inventario> getDao() {
		return dao;
	}
}