package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.PersonaDao;
import com.bellaUtopia.entidad.Persona;

public class PersonaService extends ServiceBase<Persona, DaoBase<Persona>> {

	@Inject
	private PersonaDao dao;

	@Override
	public DaoBase<Persona> getDao() {
		return dao;
	}
}