package com.bellaUtopia.servicio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.bellaUtopia.dao.UsuarioDao;
import com.bellaUtopia.entidad.Usuario;
import com.bellaUtopia.excepciones.ApplicationException;
import com.bellaUtopia.excepciones.ManejoError;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

@Stateless
public class UsuarioService extends ServiceBase<Usuario, DaoBase<Usuario>> {

	@PersistenceContext
	public EntityManager em;

	@Inject
	private UsuarioDao dao;

	@Override
	public DaoBase<Usuario> getDao() {
		return dao;
	}

	public EntityManager getEntityManager() {
		return em;
	}
	
	public void eliminarByString(String usuario) throws ApplicationException {
		try {
			Usuario o = (Usuario) getEntityManager().find(Usuario.class, usuario);
			if (o == null) {
				throw new ApplicationException(ManejoError.ERROR_OBJETO_NULL);
			}
			getEntityManager().remove(o);
		} catch (Exception exp) {
			throw new ApplicationException(ManejoError.ERROR_ELIMINAR, exp.getMessage());
		}
	}
}