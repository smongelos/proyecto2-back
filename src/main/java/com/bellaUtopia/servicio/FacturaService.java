package com.bellaUtopia.servicio;

import java.util.List;

import javax.inject.Inject;

import com.bellaUtopia.dao.FacturaDao;
import com.bellaUtopia.entidad.Factura;
import com.bellaUtopia.entidad.ServicioRealizado;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

public class FacturaService extends ServiceBase<Factura, DaoBase<Factura>> {

	@Inject
	private FacturaDao dao;

	@Override
	public DaoBase<Factura> getDao() {
		return dao;
	}
	
	public Factura getTotalMonto (Integer idFactura){
		
		//Factura facturas = new Factura();

		Factura facturas = dao.obtenerEntidad(idFactura, Factura.class );
		
		return facturas;
		
	}
}