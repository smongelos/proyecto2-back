package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.RubroDao;
import com.bellaUtopia.entidad.Rubro;

public class RubroService extends ServiceBase<Rubro, DaoBase<Rubro>> {

	@Inject
	private RubroDao dao;

	@Override
	public DaoBase<Rubro> getDao() {
		return dao;
	}
}