package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.ProductoDao;
import com.bellaUtopia.entidad.Producto;

public class ProductoService extends ServiceBase<Producto, DaoBase<Producto>> {

	@Inject
	private ProductoDao dao;

	@Override
	public DaoBase<Producto> getDao() {
		return dao;
	}
}