package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.InventarioDetallesDao;
import com.bellaUtopia.entidad.InventarioDetalles;

public class InventarioDetallesService extends ServiceBase<InventarioDetalles, DaoBase<InventarioDetalles>> {

	@Inject
	private InventarioDetallesDao dao;

	@Override
	public DaoBase<InventarioDetalles> getDao() {
		return dao;
	}
}