package com.bellaUtopia.servicio;

import java.util.List;

import javax.inject.Inject;

import com.bellaUtopia.dao.ProductoDao;
import com.bellaUtopia.dao.ServicioDao;
import com.bellaUtopia.dao.ServicioProductosDao;
import com.bellaUtopia.dao.ServicioRealizadoDao;
import com.bellaUtopia.dao.ServicioRealizadoDetallesDao;
import com.bellaUtopia.entidad.Producto;
import com.bellaUtopia.entidad.Servicio;
import com.bellaUtopia.entidad.ServicioProductos;
import com.bellaUtopia.entidad.ServicioRealizado;
import com.bellaUtopia.entidad.ServicioRealizadoDetalles;
import com.bellaUtopia.excepciones.ApplicationException;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

public class ServicioRealizadoDetallesService extends ServiceBase<ServicioRealizadoDetalles, DaoBase<ServicioRealizadoDetalles>> {

	@Inject
	private ServicioRealizadoDetallesDao dao;
	
	@Inject
	private ServicioProductosDao servicioProductosDao;
	
	@Inject
	private ProductoDao productoDao;

	@Inject
	private ServicioRealizadoDao servicioRealizadoDao;

	@Inject
	private ServicioDao servicioDao;

	@Override
	public DaoBase<ServicioRealizadoDetalles> getDao() {
		return dao;
	}
	
	
	public ServicioRealizadoDetalles insertarServicioRealizadoDetales(ServicioRealizadoDetalles servicioRealizadoDetalles ) {
		
		this.verificarStock(servicioRealizadoDetalles);
		this.actualizarStock(servicioRealizadoDetalles);		
		
		dao.insert(servicioRealizadoDetalles);
		
		ServicioRealizado servicioRealizado = servicioRealizadoDao.obtenerEntidad(servicioRealizadoDetalles.getIdServicioRealizado(), ServicioRealizado.class );
		Servicio servicio = servicioDao.obtenerEntidad(servicioRealizadoDetalles.getIdServicio(), Servicio.class);		
		servicioRealizado.setMontoTotal(servicioRealizado.getMontoTotal() + servicio.getPrecio());
		
		servicioRealizadoDao.modificar(servicioRealizado);	
		
		
		return servicioRealizadoDetalles;
	}
	
	
	Boolean verificarStock(ServicioRealizadoDetalles servicioRealizadoDetalles){
		ServicioProductos servicioProductos = new ServicioProductos();
		servicioProductos.setIdServicio(servicioRealizadoDetalles.getIdServicio());
		List<ServicioProductos> listServicioProductos = servicioProductosDao.listarFiltrado(servicioProductos);
		
		Producto producto;
		for(ServicioProductos data : listServicioProductos){
			producto = productoDao.obtenerEntidad(data.getIdProducto(), Producto.class);
			if(producto.getStockActual() - data.getCantProducto() < 0 ){
				throw new ApplicationException(" Error al procesar la operación","No existe suficiente stock para el producto: " + producto.getDescripcion());
			}
		}
		
		return true;
	}
	
	Boolean actualizarStock(ServicioRealizadoDetalles servicioRealizadoDetalles){
		ServicioProductos servicioProductos = new ServicioProductos();
		servicioProductos.setIdServicio(servicioRealizadoDetalles.getIdServicio());
		List<ServicioProductos> listServicioProductos = servicioProductosDao.listarFiltrado(servicioProductos);
		
		Producto producto;
		for(ServicioProductos data : listServicioProductos){
			producto = productoDao.obtenerEntidad(data.getIdProducto(), Producto.class);
			producto.setStockActual(producto.getStockActual()-data.getCantProducto());
			productoDao.modificar(producto);
		}
		
		return true;
	}
	
	public ServicioRealizadoDetalles eliminarServicioRealizadoDetales(ServicioRealizadoDetalles servicioRealizadoDetalles ) {
		
		this.restaurarStock(servicioRealizadoDetalles);		
		
		ServicioRealizado servicioRealizado = servicioRealizadoDao.obtenerEntidad(servicioRealizadoDetalles.getIdServicioRealizado(), ServicioRealizado.class );
		Servicio servicio = servicioDao.obtenerEntidad(servicioRealizadoDetalles.getIdServicio(), Servicio.class);		
		servicioRealizado.setMontoTotal(servicioRealizado.getMontoTotal() - servicio.getPrecio());
		
		servicioRealizadoDao.modificar(servicioRealizado);	

		dao.eliminarEntidad(servicioRealizadoDetalles.getIdServicioRealizadoDetalles(), ServicioRealizadoDetalles.class);
		
		return servicioRealizadoDetalles;
	}
	
	Boolean restaurarStock(ServicioRealizadoDetalles servicioRealizadoDetalles){
		ServicioProductos servicioProductos = new ServicioProductos();
		servicioProductos.setIdServicio(servicioRealizadoDetalles.getIdServicio());
		List<ServicioProductos> listServicioProductos = servicioProductosDao.listarFiltrado(servicioProductos);
		
		Producto producto;
		for(ServicioProductos data : listServicioProductos){
			producto = productoDao.obtenerEntidad(data.getIdProducto(), Producto.class);
			producto.setStockActual(producto.getStockActual()+data.getCantProducto());
			productoDao.modificar(producto);
		}
		
		return true;
	}
}