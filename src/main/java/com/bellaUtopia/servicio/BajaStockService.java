package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.BajaStockDao;
import com.bellaUtopia.entidad.BajaStock;

public class BajaStockService extends ServiceBase<BajaStock, DaoBase<BajaStock>> {

	@Inject
	private BajaStockDao dao;

	@Override
	public DaoBase<BajaStock> getDao() {
		return dao;
	}
}