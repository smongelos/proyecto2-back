package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.EstadosDao;
import com.bellaUtopia.entidad.Estados;

public class EstadosService extends ServiceBase<Estados, DaoBase<Estados>> {

	@Inject
	private EstadosDao dao;

	@Override
	public DaoBase<Estados> getDao() {
		return dao;
	}
}