package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.ServicioProductosDao;
import com.bellaUtopia.entidad.ServicioProductos;

public class ServicioProductosService extends ServiceBase<ServicioProductos, DaoBase<ServicioProductos>> {

	@Inject
	private ServicioProductosDao dao;

	@Override
	public DaoBase<ServicioProductos> getDao() {
		return dao;
	}
}