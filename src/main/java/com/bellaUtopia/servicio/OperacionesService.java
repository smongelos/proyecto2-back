package com.bellaUtopia.servicio;

import javax.inject.Inject;

import com.bellaUtopia.dao.OperacionesDao;
import com.bellaUtopia.entidad.Caja;
import com.bellaUtopia.entidad.Operaciones;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;

public class OperacionesService extends ServiceBase<Operaciones, DaoBase<Operaciones>> {

	@Inject
	private OperacionesDao dao;

	@Inject
	private CajaService service;

	@Override
	public DaoBase<Operaciones> getDao() {
		return dao;
	}

	public Operaciones insertarOperaciones(Operaciones operaciones) {
		dao.insert(operaciones);
		Caja caja = service.obtenerEntidad(operaciones.getIdCaja(), Caja.class);

		if (operaciones.getTipoOperacion().equals("S")) {
			int monto = caja.getMontoTotalEgreso() + operaciones.getMonto();
			caja.setMontoTotalEgreso(monto);
		} else {
			int monto = caja.getMontoTotalIngreso() + operaciones.getMonto();
			caja.setMontoTotalIngreso(monto);
		}
		service.modificar(caja);

		return operaciones;
	}

	public Operaciones eliminarOperaciones(Operaciones operaciones) {
		
		Caja caja = service.obtenerEntidad(operaciones.getIdCaja(), Caja.class);

		if (operaciones.getTipoOperacion().equals("S")) {
			int monto = caja.getMontoTotalEgreso() - operaciones.getMonto();
			caja.setMontoTotalEgreso(monto);
		} else {
			int monto = caja.getMontoTotalIngreso() - operaciones.getMonto();
			caja.setMontoTotalIngreso(monto);
		}
		service.modificar(caja);
		dao.eliminarEntidad(operaciones.getIdOperaciones(), Operaciones.class );
		return operaciones;
	}
}
