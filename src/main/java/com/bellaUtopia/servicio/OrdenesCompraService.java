package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.OrdenesCompraDao;
import com.bellaUtopia.entidad.OrdenesCompra;

public class OrdenesCompraService extends ServiceBase<OrdenesCompra, DaoBase<OrdenesCompra>> {

	@Inject
	private OrdenesCompraDao dao;

	@Override
	public DaoBase<OrdenesCompra> getDao() {
		return dao;
	}
}