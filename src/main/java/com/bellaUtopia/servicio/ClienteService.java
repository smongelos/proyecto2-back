package com.bellaUtopia.servicio;

import javax.inject.Inject;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.dao.ClienteDao;
import com.bellaUtopia.entidad.Cliente;

public class ClienteService extends ServiceBase<Cliente, DaoBase<Cliente>> {

	@Inject
	private ClienteDao dao;

	@Override
	public DaoBase<Cliente> getDao() {
		return dao;
	}
}