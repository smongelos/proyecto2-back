package com.bellaUtopia.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bellaUtopia.entidad.Operaciones;
import com.bellaUtopia.servicio.OperacionesService;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/operaciones")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OperacionesResource extends ResourceBase<Operaciones, ServiceBase<Operaciones, DaoBase<Operaciones>>> {

	@Inject
	private OperacionesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Operaciones getEntity() {
		return new Operaciones();
	}

	@Override
	protected Class<Operaciones> getEntityKeyType() {
		return Operaciones.class;
	}
	
	@POST
	@Path("/insertar-operaciones")
	public Response insertarEmpleadoDetalles(Operaciones operaciones){
		return Response.ok( service.insertarOperaciones(operaciones)).build();
	}	
	
	@POST
	@Path("/eliminar-operaciones")
	public Response eliminarOperaciones(Operaciones operaciones){
		return Response.ok( service.eliminarOperaciones(operaciones)).build();
	}
}