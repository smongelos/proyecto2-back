package com.bellaUtopia.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bellaUtopia.entidad.Empleado;
import com.bellaUtopia.param.EmpleadoDetallesParam;
import com.bellaUtopia.servicio.EmpleadoService;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/empleado")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadoResource extends ResourceBase<Empleado, ServiceBase<Empleado, DaoBase<Empleado>>> {

	@Inject
	private EmpleadoService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Empleado getEntity() {
		return new Empleado();
	}

	@Override
	protected Class<Empleado> getEntityKeyType() {
		return Empleado.class;
	}
	
	@POST
	@Path("/insertar-empleado-detalles")
	public Response insertarEmpleadoDetalles(EmpleadoDetallesParam empleadoDetallesParam){
		return Response.ok( service.insertarEmpleadoDetalles(empleadoDetallesParam)).build();
	}	
	
	@PUT
	@Path("/modificar-empleado-detalles")
	public Response modificarEmpleadoDetalles(EmpleadoDetallesParam empleadoDetallesParam){
		return Response.ok( service.modificarEmpleadoDetalles(empleadoDetallesParam)).build();
	}
}