package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.OrdenesCompraService;
import com.bellaUtopia.entidad.OrdenesCompra;

@Named
@RequestScoped
@Path("/rest/ordenes-compra")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesCompraResource extends ResourceBase<OrdenesCompra, ServiceBase<OrdenesCompra, DaoBase<OrdenesCompra>>> {

	@Inject
	private OrdenesCompraService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public OrdenesCompra getEntity() {
		return new OrdenesCompra();
	}

	@Override
	protected Class<OrdenesCompra> getEntityKeyType() {
		return OrdenesCompra.class;
	}
}