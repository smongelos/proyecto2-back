package com.bellaUtopia.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bellaUtopia.entidad.ServicioRealizado;
import com.bellaUtopia.param.CobranzaParam;
import com.bellaUtopia.servicio.ServicioRealizadoService;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/servicio-realizado")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServicioRealizadoResource extends ResourceBase<ServicioRealizado, ServiceBase<ServicioRealizado, DaoBase<ServicioRealizado>>> {

	@Inject
	private ServicioRealizadoService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ServicioRealizado getEntity() {
		return new ServicioRealizado();
	}

	@Override
	protected Class<ServicioRealizado> getEntityKeyType() {
		return ServicioRealizado.class;
	}
	
	@POST
	@Path("/realizar-pago")
	public Response realizarPago(CobranzaParam cobranzaParam){
		return Response.ok( service.realizarPago(cobranzaParam)).build();
	}	
}