package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.OrdenesCompraDetallesService;
import com.bellaUtopia.entidad.OrdenesCompraDetalles;

@Named
@RequestScoped
@Path("/rest/ordenes-compra-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesCompraDetallesResource extends ResourceBase<OrdenesCompraDetalles, ServiceBase<OrdenesCompraDetalles, DaoBase<OrdenesCompraDetalles>>> {

	@Inject
	private OrdenesCompraDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public OrdenesCompraDetalles getEntity() {
		return new OrdenesCompraDetalles();
	}

	@Override
	protected Class<OrdenesCompraDetalles> getEntityKeyType() {
		return OrdenesCompraDetalles.class;
	}
}