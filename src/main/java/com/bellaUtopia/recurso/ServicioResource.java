package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.ServicioService;
import com.bellaUtopia.entidad.Servicio;

@Named
@RequestScoped
@Path("/rest/servicio")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServicioResource extends ResourceBase<Servicio, ServiceBase<Servicio, DaoBase<Servicio>>> {

	@Inject
	private ServicioService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Servicio getEntity() {
		return new Servicio();
	}

	@Override
	protected Class<Servicio> getEntityKeyType() {
		return Servicio.class;
	}
}