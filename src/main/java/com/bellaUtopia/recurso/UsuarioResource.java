package com.bellaUtopia.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bellaUtopia.entidad.Usuario;
import com.bellaUtopia.servicio.UsuarioService;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/usuario")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioResource extends ResourceBase<Usuario, ServiceBase<Usuario, DaoBase<Usuario>>> {

	@Inject
	private UsuarioService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Usuario getEntity() {
		return new Usuario();
	}

	@Override
	protected Class<Usuario> getEntityKeyType() {
		return Usuario.class;
	}
	
	@DELETE
	@Path("/eliminar/{usuario}")
	public void eliminar(@PathParam("usuario") String usuario){
		service.eliminarByString(usuario);
	}
}