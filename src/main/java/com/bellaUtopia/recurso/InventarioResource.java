package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.InventarioService;
import com.bellaUtopia.entidad.Inventario;

@Named
@RequestScoped
@Path("/rest/inventario")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InventarioResource extends ResourceBase<Inventario, ServiceBase<Inventario, DaoBase<Inventario>>> {

	@Inject
	private InventarioService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Inventario getEntity() {
		return new Inventario();
	}

	@Override
	protected Class<Inventario> getEntityKeyType() {
		return Inventario.class;
	}
}