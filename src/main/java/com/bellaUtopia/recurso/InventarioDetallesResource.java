package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.InventarioDetallesService;
import com.bellaUtopia.entidad.InventarioDetalles;

@Named
@RequestScoped
@Path("/rest/inventario-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InventarioDetallesResource extends ResourceBase<InventarioDetalles, ServiceBase<InventarioDetalles, DaoBase<InventarioDetalles>>> {

	@Inject
	private InventarioDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public InventarioDetalles getEntity() {
		return new InventarioDetalles();
	}

	@Override
	protected Class<InventarioDetalles> getEntityKeyType() {
		return InventarioDetalles.class;
	}
}