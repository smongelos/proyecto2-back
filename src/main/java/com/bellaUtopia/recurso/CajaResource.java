package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.CajaService;
import com.bellaUtopia.entidad.Caja;

@Named
@RequestScoped
@Path("/rest/caja")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CajaResource extends ResourceBase<Caja, ServiceBase<Caja, DaoBase<Caja>>> {

	@Inject
	private CajaService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Caja getEntity() {
		return new Caja();
	}

	@Override
	protected Class<Caja> getEntityKeyType() {
		return Caja.class;
	}
}