package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.EmpleadoDetallesService;
import com.bellaUtopia.entidad.EmpleadoDetalles;

@Named
@RequestScoped
@Path("/rest/empleado-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadoDetallesResource extends ResourceBase<EmpleadoDetalles, ServiceBase<EmpleadoDetalles, DaoBase<EmpleadoDetalles>>> {

	@Inject
	private EmpleadoDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public EmpleadoDetalles getEntity() {
		return new EmpleadoDetalles();
	}

	@Override
	protected Class<EmpleadoDetalles> getEntityKeyType() {
		return EmpleadoDetalles.class;
	}
}