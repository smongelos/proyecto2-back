package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.ServicioProductosService;
import com.bellaUtopia.entidad.ServicioProductos;

@Named
@RequestScoped
@Path("/rest/servicio-productos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServicioProductosResource extends ResourceBase<ServicioProductos, ServiceBase<ServicioProductos, DaoBase<ServicioProductos>>> {

	@Inject
	private ServicioProductosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ServicioProductos getEntity() {
		return new ServicioProductos();
	}

	@Override
	protected Class<ServicioProductos> getEntityKeyType() {
		return ServicioProductos.class;
	}
}