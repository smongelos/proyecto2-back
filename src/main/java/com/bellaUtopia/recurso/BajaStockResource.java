package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.BajaStockService;
import com.bellaUtopia.entidad.BajaStock;

@Named
@RequestScoped
@Path("/rest/baja-stock")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BajaStockResource extends ResourceBase<BajaStock, ServiceBase<BajaStock, DaoBase<BajaStock>>> {

	@Inject
	private BajaStockService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public BajaStock getEntity() {
		return new BajaStock();
	}

	@Override
	protected Class<BajaStock> getEntityKeyType() {
		return BajaStock.class;
	}
}