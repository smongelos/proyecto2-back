package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.EstadosService;
import com.bellaUtopia.entidad.Estados;

@Named
@RequestScoped
@Path("/rest/estados")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EstadosResource extends ResourceBase<Estados, ServiceBase<Estados, DaoBase<Estados>>> {

	@Inject
	private EstadosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Estados getEntity() {
		return new Estados();
	}

	@Override
	protected Class<Estados> getEntityKeyType() {
		return Estados.class;
	}
}