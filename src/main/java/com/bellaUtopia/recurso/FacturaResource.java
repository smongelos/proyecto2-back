package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.FacturaService;
import com.bellaUtopia.entidad.Factura;

@Named
@RequestScoped
@Path("/rest/factura")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FacturaResource extends ResourceBase<Factura, ServiceBase<Factura, DaoBase<Factura>>> {

	@Inject
	private FacturaService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Factura getEntity() {
		return new Factura();
	}

	@Override
	protected Class<Factura> getEntityKeyType() {
		return Factura.class;
	}
}