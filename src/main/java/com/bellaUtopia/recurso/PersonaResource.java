package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.PersonaService;
import com.bellaUtopia.entidad.Persona;

@Named
@RequestScoped
@Path("/rest/persona")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PersonaResource extends ResourceBase<Persona, ServiceBase<Persona, DaoBase<Persona>>> {

	@Inject
	private PersonaService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Persona getEntity() {
		return new Persona();
	}

	@Override
	protected Class<Persona> getEntityKeyType() {
		return Persona.class;
	}
}