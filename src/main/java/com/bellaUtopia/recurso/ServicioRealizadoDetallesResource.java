package com.bellaUtopia.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bellaUtopia.entidad.ServicioRealizadoDetalles;
import com.bellaUtopia.servicio.ServicioRealizadoDetallesService;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/servicio-realizado-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServicioRealizadoDetallesResource extends ResourceBase<ServicioRealizadoDetalles, ServiceBase<ServicioRealizadoDetalles, DaoBase<ServicioRealizadoDetalles>>> {

	@Inject
	private ServicioRealizadoDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ServicioRealizadoDetalles getEntity() {
		return new ServicioRealizadoDetalles();
	}

	@Override
	protected Class<ServicioRealizadoDetalles> getEntityKeyType() {
		return ServicioRealizadoDetalles.class;
	}
	
	@POST
	@Path("/insertar-servicio-realizado-detalles")
	public Response insertarServicioRealizadoDetales(ServicioRealizadoDetalles servicioRealizadoDetalles){
		return Response.ok( service.insertarServicioRealizadoDetales(servicioRealizadoDetalles)).build();
	}	
	
	@POST
	@Path("/eliminar-servicio-realizado-detalles")
	public Response eliminarServicioRealizadoDetales(ServicioRealizadoDetalles servicioRealizadoDetalles){
		return Response.ok( service.eliminarServicioRealizadoDetales(servicioRealizadoDetalles)).build();
	}	
}