package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.ProveedorService;
import com.bellaUtopia.entidad.Proveedor;

@Named
@RequestScoped
@Path("/rest/proveedor")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProveedorResource extends ResourceBase<Proveedor, ServiceBase<Proveedor, DaoBase<Proveedor>>> {

	@Inject
	private ProveedorService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Proveedor getEntity() {
		return new Proveedor();
	}

	@Override
	protected Class<Proveedor> getEntityKeyType() {
		return Proveedor.class;
	}
}