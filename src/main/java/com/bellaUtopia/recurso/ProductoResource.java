package com.bellaUtopia.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bellaUtopia.util.DaoBase;
import com.bellaUtopia.util.ResourceBase;
import com.bellaUtopia.util.ServiceBase;
import com.bellaUtopia.servicio.ProductoService;
import com.bellaUtopia.entidad.Producto;

@Named
@RequestScoped
@Path("/rest/producto")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductoResource extends ResourceBase<Producto, ServiceBase<Producto, DaoBase<Producto>>> {

	@Inject
	private ProductoService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Producto getEntity() {
		return new Producto();
	}

	@Override
	protected Class<Producto> getEntityKeyType() {
		return Producto.class;
	}
}