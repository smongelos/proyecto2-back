package com.bellaUtopia.param;

import java.util.Date;

public class EmpleadoDetallesParam {
	private Integer idEmpleado;
	private Integer idEmpleadoDetalles;
	private String usuario;
	private Integer idPersona;
	private Integer idRubro;
	private Integer salario;
	private Date fechaAsignacion;
	private Date fechaIngreso;
	private Date fechaSalida;
	
	
	public Integer getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public Integer getIdEmpleadoDetalles() {
		return idEmpleadoDetalles;
	}
	public void setIdEmpleadoDetalles(Integer idEmpleadoDetalles) {
		this.idEmpleadoDetalles = idEmpleadoDetalles;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}
	public Integer getIdRubro() {
		return idRubro;
	}
	public void setIdRubro(Integer idRubro) {
		this.idRubro = idRubro;
	}
	public Integer getSalario() {
		return salario;
	}
	public void setSalario(Integer salario) {
		this.salario = salario;
	}
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}
	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Date getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}
}
