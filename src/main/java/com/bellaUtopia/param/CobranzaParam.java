package com.bellaUtopia.param;

import com.bellaUtopia.entidad.Caja;
import com.bellaUtopia.entidad.Factura;
import com.bellaUtopia.entidad.ServicioRealizado;

public class CobranzaParam {
	private ServicioRealizado servicioRealizado;
	private Caja caja;
	private Factura factura;

	public ServicioRealizado getServicioRealizado() {
		return servicioRealizado;
	}

	public void setServicioRealizado(ServicioRealizado servicioRealizado) {
		this.servicioRealizado = servicioRealizado;
	}

	public Caja getCaja() {
		return caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

}
