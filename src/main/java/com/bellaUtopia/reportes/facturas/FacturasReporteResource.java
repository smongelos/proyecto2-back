package com.bellaUtopia.reportes.facturas;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.bellaUtopia.entidad.Factura;
import com.bellaUtopia.servicio.FacturaService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;

@Named
@RequestScoped
@Path("/rest/reportes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.WILDCARD)
public class FacturasReporteResource {

	@Inject
	private FacturaService facturasService;


	@POST
	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.WILDCARD)
	@Path("/facturacion")
	public Response facturacion(@Context UriInfo uriInfo, 
            final @Context HttpServletRequest httpServletRequest,  final @Context ServletContext servletContext) throws JRException {
		System.out.println("Ingreso a servicio");

		String nombreJasper = "rp_factura_contado_fijo_cf.jasper";
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reportes/" + nombreJasper);
		
		MultivaluedMap<String, String> uriParam = uriInfo.getQueryParameters();
		Integer idFactura = Integer.parseInt(uriParam.get("facturaId").get(0));

		System.out.println("Llamada a getTotalMonto");
		Factura factura = facturasService.getTotalMonto(idFactura);
		System.out.println("Llamada a ");
		String montoConIva = facturasService.numberToString(factura.getMontoTotal());
		
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("subReportPath", this.getClass().getClassLoader().getResource("/reportes/").toString());
		parametros.put("facturaId", idFactura);
		parametros.put("montoConIva", montoConIva);

		Connector conn = new Connector();
		byte[] pdf = JasperRunManager.runReportToPdf(is, parametros, conn.getConnection());
		
		return Response.ok().header("content-disposition", "inline")
				.header(HttpHeaders.CACHE_CONTROL, "no-cache")
				.header(HttpHeaders.CONTENT_TYPE, "application/pdf")
				.entity(pdf).build();
    }
	
	
	@POST
	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.WILDCARD)
	@Path("/ordenes-compras")
	public Response ordenesCompras(@Context UriInfo uriInfo, 
            final @Context HttpServletRequest httpServletRequest,  final @Context ServletContext servletContext) throws JRException {

		String nombreJasper = "rp_orden_compra.jasper";
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reportes/" + nombreJasper);
		
		MultivaluedMap<String, String> uriParam = uriInfo.getQueryParameters();
		Long idOrdenesCompra = Long.parseLong(uriParam.get("idOrdenesCompra").get(0));
		
//		Facturas facturas = facturasService.getTotalMonto(factura);
//		String montoConIva = facturasService.numberToString(facturas.getMontoConIva());
//		
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("subReportPath", this.getClass().getClassLoader().getResource("/reportes/").toString());
		parametros.put("idOrdenesCompra", idOrdenesCompra);

		Connector conn = new Connector();
		byte[] pdf = JasperRunManager.runReportToPdf(is, parametros, conn.getConnection());
		
		return Response.ok().header("content-disposition", "inline")
				.header(HttpHeaders.CACHE_CONTROL, "no-cache")
				.header(HttpHeaders.CONTENT_TYPE, "application/pdf")
				.entity(pdf).build();
    }
	
	
	@POST
	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.WILDCARD)
	@Path("/ordenes-remision")
	public Response ordenesRemision(@Context UriInfo uriInfo, 
            final @Context HttpServletRequest httpServletRequest,  final @Context ServletContext servletContext) throws JRException {

		String nombreJasper = "rp_orden_remision.jasper";
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("/reportes/" + nombreJasper);
		
		MultivaluedMap<String, String> uriParam = uriInfo.getQueryParameters();
		Long idOrdenesRemision = Long.parseLong(uriParam.get("idOrdenesRemision").get(0));
		
//		Facturas facturas = facturasService.getTotalMonto(factura);
//		String montoConIva = facturasService.numberToString(facturas.getMontoConIva());
//		
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("subReportPath", this.getClass().getClassLoader().getResource("/reportes/").toString());
		parametros.put("idOrdenesRemision", idOrdenesRemision
				);

		Connector conn = new Connector();
		byte[] pdf = JasperRunManager.runReportToPdf(is, parametros, conn.getConnection());
		
		return Response.ok().header("content-disposition", "inline")
				.header(HttpHeaders.CACHE_CONTROL, "no-cache")
				.header(HttpHeaders.CONTENT_TYPE, "application/pdf")
				.entity(pdf).build();
    }
}
