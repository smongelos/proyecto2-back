package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "rubro", schema = "public")
public class Rubro extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "RUBRO_ID_RUBRO_SEQ", allocationSize=1, initialValue=1, sequenceName = "rubro_id_rubro_seq_1")	
	@GeneratedValue(generator = "RUBRO_ID_RUBRO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_rubro")
	private Integer idRubro;

	@Column(name = "descripcion")
	private String descripcion;


	public Integer getIdRubro(){
		return idRubro;
	}
	public void setIdRubro(Integer idRubro){
		this.idRubro = idRubro;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}