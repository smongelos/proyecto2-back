package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "inventario", schema = "public")
public class Inventario extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "INVENTARIO_ID_INVENTARIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "inventario_id_inventario_seq")	
	@GeneratedValue(generator = "INVENTARIO_ID_INVENTARIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_inventario")
	private Integer idInventario;

	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "fecha")
	private Date fecha;


	public Integer getIdInventario(){
		return idInventario;
	}
	public void setIdInventario(Integer idInventario){
		this.idInventario = idInventario;
	}
	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}

}