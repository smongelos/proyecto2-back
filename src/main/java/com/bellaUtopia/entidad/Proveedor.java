package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "proveedor", schema = "public")
public class Proveedor extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PROVEEDOR_ID_PROVEEDOR_SEQ", allocationSize=1, initialValue=1, sequenceName = "proveedor_id_proveedor_seq_1")	
	@GeneratedValue(generator = "PROVEEDOR_ID_PROVEEDOR_SEQ", strategy=GenerationType.SEQUENCE)


	@Id
	@Column(name = "id_proveedor")
	private Integer idProveedor;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "ruc")
	private String ruc;


	public Integer getIdProveedor(){
		return idProveedor;
	}
	public void setIdProveedor(Integer idProveedor){
		this.idProveedor = idProveedor;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getTelefono(){
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	public String getDireccion(){
		return direccion;
	}
	public void setDireccion(String direccion){
		this.direccion = direccion;
	}
	public String getRuc(){
		return ruc;
	}
	public void setRuc(String ruc){
		this.ruc = ruc;
	}

}