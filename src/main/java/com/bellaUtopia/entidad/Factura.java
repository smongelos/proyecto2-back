package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "factura", schema = "public")
public class Factura extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "FACTURA_ID_FACTURA_SEQ", allocationSize=1, initialValue=1, sequenceName = "factura_id_factura_seq")	
	@GeneratedValue(generator = "FACTURA_ID_FACTURA_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_factura")
	private Integer idFactura;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "monto_total")
	private Integer montoTotal;

	@Column(name = "iva5")
	private Integer iva5;

	@Column(name = "iva10")
	private Integer iva10;

	@Column(name = "exenta")
	private Integer exenta;

	@Column(name = "id_caja")
	private Integer idCaja;

	@Column(name = "id_servicio_realizado")
	private Integer idServicioRealizado;


	public Integer getIdFactura(){
		return idFactura;
	}
	public void setIdFactura(Integer idFactura){
		this.idFactura = idFactura;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}
	public Integer getMontoTotal(){
		return montoTotal;
	}
	public void setMontoTotal(Integer montoTotal){
		this.montoTotal = montoTotal;
	}
	public Integer getIva5(){
		return iva5;
	}
	public void setIva5(Integer iva5){
		this.iva5 = iva5;
	}
	public Integer getIva10(){
		return iva10;
	}
	public void setIva10(Integer iva10){
		this.iva10 = iva10;
	}
	public Integer getExenta(){
		return exenta;
	}
	public void setExenta(Integer exenta){
		this.exenta = exenta;
	}
	public Integer getIdCaja(){
		return idCaja;
	}
	public void setIdCaja(Integer idCaja){
		this.idCaja = idCaja;
	}
	public Integer getIdServicioRealizado(){
		return idServicioRealizado;
	}
	public void setIdServicioRealizado(Integer idServicioRealizado){
		this.idServicioRealizado = idServicioRealizado;
	}

}