package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "servicio_productos", schema = "public")
public class ServicioProductos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "SERVICIO_PRODUCTO_ID_SERVICIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "servicio_productos_id_servicio_productos_seq")	
	@GeneratedValue(generator = "SERVICIO_PRODUCTO_ID_SERVICIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_servicio_productos")
	private Integer idServicioProductos;

	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "id_servicio")
	private Integer idServicio;
	
	@Column(name = "cant_producto")
	private Integer cantProducto;

	@Column(name = "costo_producto")
	private Integer costoProducto;


	public Integer getIdServicioProductos(){
		return idServicioProductos;
	}
	public void setIdServicioProductos(Integer idServicioProductos){
		this.idServicioProductos = idServicioProductos;
	}
	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public Integer getIdServicio(){
		return idServicio;
	}
	public void setIdServicio(Integer idServicio){
		this.idServicio = idServicio;
	}
	
	public Integer getCantProducto(){
		return cantProducto;
	}
	public void setCantProducto(Integer cantProducto){
		this.cantProducto = cantProducto;
	}	
	
	public Integer getCostoProducto(){
		return costoProducto;
	}
	public void setCostoProducto(Integer costoProducto){
		this.costoProducto= costoProducto;
	}

}