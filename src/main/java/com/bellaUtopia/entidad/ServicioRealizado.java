package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "servicio_realizado", schema = "public")
public class ServicioRealizado extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "SERVICIO_REALIZADO_ID_SERVICIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "servicio_realizado_id_servicio_realizado_seq")	
	@GeneratedValue(generator = "SERVICIO_REALIZADO_ID_SERVICIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_servicio_realizado")
	private Integer idServicioRealizado;

	@Column(name = "fecha_operacion")
	private Date fechaOperacion;

	@Column(name = "monto_total")
	private Integer montoTotal;

	@Column(name = "id_cliente")
	private Integer idCliente;
	
	@Column(name = "pagado")
	private Boolean pagado;


	public Integer getIdServicioRealizado(){
		return idServicioRealizado;
	}
	public void setIdServicioRealizado(Integer idServicioRealizado){
		this.idServicioRealizado = idServicioRealizado;
	}
	public Date getFechaOperacion(){
		return fechaOperacion;
	}
	public void setFechaOperacion(Date fechaOperacion){
		this.fechaOperacion = fechaOperacion;
	}
	public Integer getMontoTotal(){
		return montoTotal;
	}
	public void setMontoTotal(Integer montoTotal){
		this.montoTotal = montoTotal;
	}
	public Integer getIdCliente(){
		return idCliente;
	}
	public void setIdCliente(Integer idCliente){
		this.idCliente = idCliente;
	}
	public Boolean getPagado() {
		return pagado;
	}
	public void setPagado(Boolean pagado) {
		this.pagado = pagado;
	}

}