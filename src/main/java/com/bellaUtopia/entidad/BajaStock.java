package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "baja_stock", schema = "public")
public class BajaStock extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "BAJA_STOCK_ID_BAJA_STOCK_SEQ", allocationSize=1, initialValue=1, sequenceName = "baja_stock_id_baja_stock_seq")	
	@GeneratedValue(generator = "BAJA_STOCK_ID_BAJA_STOCK_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_baja_stock")
	private Integer idBajaStock;

	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "motivo_baja")
	private String motivoBaja;
	
	@Column(name = "cantidad_baja")
	private Integer cantidadBaja;


	public Integer getIdBajaStock(){
		return idBajaStock;
	}
	public void setIdBajaStock(Integer idBajaStock){
		this.idBajaStock = idBajaStock;
	}
	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}
	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}
	public String getMotivoBaja(){
		return motivoBaja;
	}
	public void setMotivoBaja(String motivoBaja){
		this.motivoBaja = motivoBaja;
	}
	public Integer getCantidadBaja(){
		return cantidadBaja;
	}
	public void setCantidadBaja(Integer cantidadBaja){
		this.cantidadBaja = cantidadBaja;
	}
}