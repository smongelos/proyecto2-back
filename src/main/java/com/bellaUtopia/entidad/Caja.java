package com.bellaUtopia.entidad;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "caja", schema = "public")
public class Caja extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "CAJA_ID_CAJA_SEQ", allocationSize=1, initialValue=1, sequenceName = "caja_id_caja_seq")	
	@GeneratedValue(generator = "CAJA_ID_CAJA_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_caja")
	private Integer idCaja;

	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "fecha")
    @Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name = "monto_total_ingreso")
	private Integer montoTotalIngreso;

	@Column(name = "monto_total_egreso")
	private Integer montoTotalEgreso;
	
	@Column(name = "estado")
	private Boolean estado;


	public Integer getIdCaja(){
		return idCaja;
	}
	public void setIdCaja(Integer idCaja){
		this.idCaja = idCaja;
	}
	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}
	public Integer getMontoTotalIngreso(){
		return montoTotalIngreso;
	}
	public void setMontoTotalIngreso(Integer montoTotalIngreso){
		this.montoTotalIngreso = montoTotalIngreso;
	}
	public Integer getMontoTotalEgreso(){
		return montoTotalEgreso;
	}
	public void setMontoTotalEgreso(Integer montoTotalEgreso){
		this.montoTotalEgreso = montoTotalEgreso;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}