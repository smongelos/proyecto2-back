package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "persona", schema = "public")
public class Persona extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PERSONA_ID_PERSONA_SEQ", allocationSize=1, initialValue=1, sequenceName = "persona_id_persona_seq")	
	@GeneratedValue(generator = "PERSONA_ID_PERSONA_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_persona")
	private Integer idPersona;

	@Column(name = "documento")
	private String documento;

	@Column(name = "nombres")
	private String nombres;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "domicilio")
	private String domicilio;

	@Column(name = "email")
	private String email;


	public Integer getIdPersona(){
		return idPersona;
	}
	public void setIdPersona(Integer idPersona){
		this.idPersona = idPersona;
	}
	public String getDocumento(){
		return documento;
	}
	public void setDocumento(String documento){
		this.documento = documento;
	}
	public String getNombres(){
		return nombres;
	}
	public void setNombres(String nombres){
		this.nombres = nombres;
	}
	public String getApellidos(){
		return apellidos;
	}
	public void setApellidos(String apellidos){
		this.apellidos = apellidos;
	}
	public Date getFechaNacimiento(){
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento){
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getTelefono(){
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	public String getDomicilio(){
		return domicilio;
	}
	public void setDomicilio(String domicilio){
		this.domicilio = domicilio;
	}
	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email = email;
	}

}