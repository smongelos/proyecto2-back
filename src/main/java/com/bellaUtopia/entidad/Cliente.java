package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "cliente", schema = "public")
public class Cliente extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "CLIENTE_ID_CLIENTE_SEQ", allocationSize=1, initialValue=1, sequenceName = "cliente_id_cliente_seq")	
	@GeneratedValue(generator = "CLIENTE_ID_CLIENTE_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_cliente")
	private Integer idCliente;

	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Column(name = "id_persona")
	private Integer idPersona;
	
	@Column(name = "ruc")
	private String ruc;


	public Integer getIdCliente(){
		return idCliente;
	}
	public void setIdCliente(Integer idCliente){
		this.idCliente = idCliente;
	}
	public Date getFechaCreacion(){
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getIdPersona(){
		return idPersona;
	}
	public void setIdPersona(Integer idPersona){
		this.idPersona = idPersona;
	}
	public String getRuc(){
		return ruc;
	}
	public void setRuc(String ruc){
		this.ruc = ruc;
	}

}