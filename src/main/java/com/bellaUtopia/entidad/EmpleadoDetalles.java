package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "empleado_detalles", schema = "public")
public class EmpleadoDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "EMPLEADO_DETALLES_ID_EMPLEADO_DETALLES_SEQ", allocationSize=1, initialValue=1, sequenceName = "empleado_detalles_id_empleado_detalles_seq")	
	@GeneratedValue(generator = "EMPLEADO_DETALLES_ID_EMPLEADO_DETALLES_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_empleado_detalles")
	private Integer idEmpleadoDetalles;

	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "salario")
	private Integer salario;

	@Column(name = "fecha_asignacion")
	private Date fechaAsignacion;

	@Column(name = "id_rubro")
	private Integer idRubro;


	public Integer getIdEmpleadoDetalles(){
		return idEmpleadoDetalles;
	}
	public void setIdEmpleadoDetalles(Integer idEmpleadoDetalles){
		this.idEmpleadoDetalles = idEmpleadoDetalles;
	}
	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}
	public Integer getSalario(){
		return salario;
	}
	public void setSalario(Integer salario){
		this.salario = salario;
	}
	public Date getFechaAsignacion(){
		return fechaAsignacion;
	}
	public void setFechaAsignacion(Date fechaAsignacion){
		this.fechaAsignacion = fechaAsignacion;
	}
	public Integer getIdRubro(){
		return idRubro;
	}
	public void setIdRubro(Integer idRubro){
		this.idRubro = idRubro;
	}

}