package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "usuario", schema = "public")
public class Usuario extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "usuario")
	private String usuario;

	@Column(name = "contrasenha")
	private String contrasenha;

	@Column(name = "salt")
	private String salt;

	@Column(name = "estado")
	private Boolean estado;


	public String getUsuario(){
		return usuario;
	}
	public void setUsuario(String usuario){
		this.usuario = usuario;
	}
	public String getContrasenha(){
		return contrasenha;
	}
	public void setContrasenha(String contrasenha){
		this.contrasenha = contrasenha;
	}
	public String getSalt(){
		return salt;
	}
	public void setSalt(String salt){
		this.salt = salt;
	}
	public Boolean getEstado(){
		return estado;
	}
	public void setEstado(Boolean estado){
		this.estado = estado;
	}

}