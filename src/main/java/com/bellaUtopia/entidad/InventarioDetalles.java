package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "inventario_detalles", schema = "public")
public class InventarioDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "INVENTARIO_DETALLES_ID_INVENTARIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "inventario_detalles_id_inventario_detalles_seq")	
	@GeneratedValue(generator = "INVENTARIO_DETALLES_ID_INVENTARIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_inventario_detalles")
	private Integer idInventarioDetalles;

	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "id_inventario")
	private Integer idInventario;

	@Column(name = "stock_actual")
	private Integer stockActual;

	@Column(name = "stock_minimo")
	private Integer stockMinimo;

	@Column(name = "realizar_compra")
	private Boolean realizarCompra;


	public Integer getIdInventarioDetalles(){
		return idInventarioDetalles;
	}
	public void setIdInventarioDetalles(Integer idInventarioDetalles){
		this.idInventarioDetalles = idInventarioDetalles;
	}
	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public Integer getIdInventario(){
		return idInventario;
	}
	public void setIdInventario(Integer idInventario){
		this.idInventario = idInventario;
	}
	public Integer getStockActual(){
		return stockActual;
	}
	public void setStockActual(Integer stockActual){
		this.stockActual = stockActual;
	}
	public Integer getStockMinimo(){
		return stockMinimo;
	}
	public void setStockMinimo(Integer stockMinimo){
		this.stockMinimo = stockMinimo;
	}
	public Boolean getRealizarCompra(){
		return realizarCompra;
	}
	public void setRealizarCompra(Boolean realizarCompra){
		this.realizarCompra = realizarCompra;
	}

}