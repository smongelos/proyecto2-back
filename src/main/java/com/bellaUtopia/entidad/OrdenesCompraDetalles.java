package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "ordenes_compra_detalles", schema = "public")
public class OrdenesCompraDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "ORDENES_COMPRA_DETALLES_ID_ORDENES_COMPRA_SEQ", allocationSize=1, initialValue=1, sequenceName = "ordenes_compra_detalles_id_ordenes_compra_detalles_seq")	
	@GeneratedValue(generator = "ORDENES_COMPRA_DETALLES_ID_ORDENES_COMPRA_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_ordenes_compra_detalles")
	private Integer idOrdenesCompraDetalles;

	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "costo")
	private Integer costo;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "fecha_vencimiento")
	private Date fechaVencimiento;

	@Column(name = "id_ordenes_compra")
	private Integer idOrdenesCompra;


	public Integer getIdOrdenesCompraDetalles(){
		return idOrdenesCompraDetalles;
	}
	public void setIdOrdenesCompraDetalles(Integer idOrdenesCompraDetalles){
		this.idOrdenesCompraDetalles = idOrdenesCompraDetalles;
	}
	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public Integer getCosto(){
		return costo;
	}
	public void setCosto(Integer costo){
		this.costo = costo;
	}
	public Integer getCantidad(){
		return cantidad;
	}
	public void setCantidad(Integer cantidad){
		this.cantidad = cantidad;
	}
	public Date getFechaVencimiento(){
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento){
		this.fechaVencimiento = fechaVencimiento;
	}
	public Integer getIdOrdenesCompra(){
		return idOrdenesCompra;
	}
	public void setIdOrdenesCompra(Integer idOrdenesCompra){
		this.idOrdenesCompra = idOrdenesCompra;
	}

}