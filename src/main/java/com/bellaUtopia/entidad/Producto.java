package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "producto", schema = "public")
public class Producto extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PRODUCTO_ID_PRODUCTO_SEQ", allocationSize=1, initialValue=1, sequenceName = "producto_id_producto_seq")	
	@GeneratedValue(generator = "PRODUCTO_ID_PRODUCTO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "marca")
	private String marca;

	@Column(name = "contenido")
	private Integer contenido;

	@Column(name = "stock_minimo")
	private Integer stockMinimo;

	@Column(name = "stock_actual")
	private Integer stockActual;

	@Column(name = "costo")
	private Integer costo;

	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public String getMarca(){
		return marca;
	}
	public void setMarca(String marca){
		this.marca = marca;
	}
	public Integer getContenido(){
		return contenido;
	}
	public void setContenido(Integer contenido){
		this.contenido = contenido;
	}
	public Integer getStockMinimo(){
		return stockMinimo;
	}
	public void setStockMinimo(Integer stockMinimo){
		this.stockMinimo = stockMinimo;
	}
	public Integer getStockActual(){
		return stockActual;
	}
	public void setStockActual(Integer stockActual){
		this.stockActual = stockActual;
	}
	public Integer getCosto(){
		return costo;
	}
	public void setCosto(Integer costo){
		this.costo = costo;
	}

}