package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "estados", schema = "public")
public class Estados extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;	
	@SequenceGenerator(name = "ESTADOS_ID_ESTADO_SEQ", allocationSize=1, initialValue=1, sequenceName = "estados_id_estado_seq_1")	
	@GeneratedValue(generator = "ESTADOS_ID_ESTADO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_estado")
	private Integer idEstado;

	@Column(name = "descripcion")
	private String descripcion;


	public Integer getIdEstado(){
		return idEstado;
	}
	public void setIdEstado(Integer idEstado){
		this.idEstado = idEstado;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}