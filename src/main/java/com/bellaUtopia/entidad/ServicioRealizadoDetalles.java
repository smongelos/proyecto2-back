package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "servicio_realizado_detalles", schema = "public")
public class ServicioRealizadoDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "SERVICIO_REALIZADO_DETALLES_ID_SERVICIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "servicio_realizado_detalles_id_servicio_realizado_detalles_seq")	
	@GeneratedValue(generator = "SERVICIO_REALIZADO_DETALLES_ID_SERVICIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_servicio_realizado_detalles")
	private Integer idServicioRealizadoDetalles;

	@Column(name = "id_servicio")
	private Integer idServicio;

	@Column(name = "id_servicio_realizado")
	private Integer idServicioRealizado;

	@Column(name = "id_empleado")
	private Integer idEmpleado;


	public Integer getIdServicioRealizadoDetalles(){
		return idServicioRealizadoDetalles;
	}
	public void setIdServicioRealizadoDetalles(Integer idServicioRealizadoDetalles){
		this.idServicioRealizadoDetalles = idServicioRealizadoDetalles;
	}
	public Integer getIdServicio(){
		return idServicio;
	}
	public void setIdServicio(Integer idServicio){
		this.idServicio = idServicio;
	}
	public Integer getIdServicioRealizado(){
		return idServicioRealizado;
	}
	public void setIdServicioRealizado(Integer idServicioRealizado){
		this.idServicioRealizado = idServicioRealizado;
	}
	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}

}