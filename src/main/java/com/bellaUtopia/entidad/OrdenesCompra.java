package com.bellaUtopia.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "ordenes_compra", schema = "public")
public class OrdenesCompra extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "ORDENES_COMPRA_ID_ORDENES_COMPRA_SEQ", allocationSize=1, initialValue=1, sequenceName = "ordenes_compra_id_ordenes_compra_seq_1")	
	@GeneratedValue(generator = "ORDENES_COMPRA_ID_ORDENES_COMPRA_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_ordenes_compra")
	private Integer idOrdenesCompra;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "id_estado")
	private Integer idEstado;

	@Column(name = "recepcionado")
	private Boolean recepcionado;

	@Column(name = "costo_estimado")
	private Integer costoEstimado;

	@Column(name = "costo_total")
	private Integer costoTotal;

	@Column(name = "id_proveedor")
	private Integer idProveedor;
	
	@Column(name = "generado")
	private Boolean generado;


	public Integer getIdOrdenesCompra(){
		return idOrdenesCompra;
	}
	public void setIdOrdenesCompra(Integer idOrdenesCompra){
		this.idOrdenesCompra = idOrdenesCompra;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}
	public Integer getIdEstado(){
		return idEstado;
	}
	public void setIdEstado(Integer idEstado){
		this.idEstado = idEstado;
	}
	public Boolean getRecepcionado(){
		return recepcionado;
	}
	public void setRecepcionado(Boolean recepcionado){
		this.recepcionado = recepcionado;
	}
	public Integer getCostoEstimado(){
		return costoEstimado;
	}
	public void setCostoEstimado(Integer costoEstimado){
		this.costoEstimado = costoEstimado;
	}
	public Integer getCostoTotal(){
		return costoTotal;
	}
	public void setCostoTotal(Integer costoTotal){
		this.costoTotal = costoTotal;
	}
	public Integer getIdProveedor(){
		return idProveedor;
	}
	public void setIdProveedor(Integer idProveedor){
		this.idProveedor = idProveedor;
	}
	public Boolean getGenerado(){
		return generado;
	}
	public void setGenerado(Boolean generado){
		this.generado = generado;
	}

}