package com.bellaUtopia.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "operaciones", schema = "public")
public class Operaciones extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "OPERACIONES_ID_OPERACIONES_SEQ", allocationSize=1, initialValue=1, sequenceName = "operaciones_id_operaciones_seq")	
	@GeneratedValue(generator = "OPERACIONES_ID_OPERACIONES_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_operaciones")
	private Integer idOperaciones;

	@Column(name = "id_caja")
	private Integer idCaja;

	@Column(name = "tipo_operacion")
	private String tipoOperacion;

	@Column(name = "id_servicio_realizado")
	private Integer idServicioRealizado;

	@Column(name = "id_ordenes_compra")
	private Integer idOrdenesCompra;

	@Column(name = "monto")
	private Integer monto;

	@Column(name = "operacion_no_especificada")
	private String operacionNoEspecificada;
	
	@Column(name = "fecha")
	private Date fecha;

	public Integer getIdOperaciones(){
		return idOperaciones;
	}
	public void setIdOperaciones(Integer idOperaciones){
		this.idOperaciones = idOperaciones;
	}
	public Integer getIdCaja(){
		return idCaja;
	}
	public void setIdCaja(Integer idCaja){
		this.idCaja = idCaja;
	}
	public String getTipoOperacion(){
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion){
		this.tipoOperacion = tipoOperacion;
	}
	public Integer getIdServicioRealizado(){
		return idServicioRealizado;
	}
	public void setIdServicioRealizado(Integer idServicioRealizado){
		this.idServicioRealizado = idServicioRealizado;
	}
	public Integer getIdOrdenesCompra(){
		return idOrdenesCompra;
	}
	public void setIdOrdenesCompra(Integer idOrdenesCompra){
		this.idOrdenesCompra = idOrdenesCompra;
	}
	public Integer getMonto(){
		return monto;
	}
	public void setMonto(Integer monto){
		this.monto = monto;
	}
	public String getOperacionNoEspecificada(){
		return operacionNoEspecificada;
	}
	public void setOperacionNoEspecificada(String operacionNoEspecificada){
		this.operacionNoEspecificada = operacionNoEspecificada;
	}
	public Date getFecha(){
		return fecha;
	}
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}

}