package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "empleado", schema = "public")
public class Empleado extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "EMPLEADO_ID_EMPLEADO_SEQ", allocationSize=1, initialValue=1, sequenceName = "empleado_id_empleado_seq")	
	@GeneratedValue(generator = "EMPLEADO_ID_EMPLEADO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_empleado")
	private Integer idEmpleado;

	@Column(name = "usuario")
	private String usuario;

	@Column(name = "fecha_ingreso")
	private Date fechaIngreso;

	@Column(name = "fecha_salida")
	private Date fechaSalida;

	@Column(name = "id_persona")
	private Integer idPersona;


	public Integer getIdEmpleado(){
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado){
		this.idEmpleado = idEmpleado;
	}
	public String getUsuario(){
		return usuario;
	}
	public void setUsuario(String usuario){
		this.usuario = usuario;
	}
	public Date getFechaIngreso(){
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso){
		this.fechaIngreso = fechaIngreso;
	}
	public Date getFechaSalida(){
		return fechaSalida;
	}
	public void setFechaSalida(Date fechaSalida){
		this.fechaSalida = fechaSalida;
	}
	public Integer getIdPersona(){
		return idPersona;
	}
	public void setIdPersona(Integer idPersona){
		this.idPersona = idPersona;
	}

}