package com.bellaUtopia.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.bellaUtopia.util.EntityBase;

@Entity
@Table(name = "servicio", schema = "public")
public class Servicio extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "SERVICIO_ID_SERVICIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "servicio_id_servicio_seq")	
	@GeneratedValue(generator = "SERVICIO_ID_SERVICIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_servicio")
	private Integer idServicio;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "precio")
	private Integer precio;

	@Column(name = "cant_producto")
	private Integer cantProducto;

	@Column(name = "costo_producto")
	private Integer costoProducto;

	@Column(name = "iva")
	private Integer iva;

	@Column(name = "id_rubro")
	private Integer idRubro;


	public Integer getIdServicio(){
		return idServicio;
	}
	public void setIdServicio(Integer idServicio){
		this.idServicio = idServicio;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public Integer getPrecio(){
		return precio;
	}
	public void setPrecio(Integer precio){
		this.precio = precio;
	}
	public Integer getCantProducto(){
		return cantProducto;
	}
	public void setCantProducto(Integer cantProducto){
		this.cantProducto = cantProducto;
	}
	public Integer getCostoProducto(){
		return costoProducto;
	}
	public void setCostoProducto(Integer costoProducto){
		this.costoProducto = costoProducto;
	}
	public Integer getIva(){
		return iva;
	}
	public void setIva(Integer iva){
		this.iva = iva;
	}
	public Integer getIdRubro(){
		return idRubro;
	}
	public void setIdRubro(Integer idRubro){
		this.idRubro = idRubro;
	}

}